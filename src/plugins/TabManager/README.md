TabManager plug-in for Bhawk
-------------------------------------------------
This plug-in adds the ability to manage tabs and windows in Blue Hawk.

![tbm3](http://i.imgur.com/Gh8bEXo.png)

**TODOs**

* Modifying `refresh` algorithm for updating changed items and not all items.
